import java.util.*;

public class Aimsprojects {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<DigitalVideoDisc> collection = new ArrayList<DigitalVideoDisc>();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("chi", "ok");
        dvd1.cost = 3;
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("chi1", "ok1");
        dvd2.cost = 4;
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("chi2", "ok2");
        dvd2.cost = 5;
        collection.add(dvd3);
        collection.add(dvd2);
        collection.add(dvd1);
        Iterator<DigitalVideoDisc> iterator = collection.iterator();
        System.out.println("--------------------------------------");
        System.out.println("The DVDs currently in the order are:");
        while (iterator.hasNext()) {
            System.out.println(iterator.next().getTitle() + " ");
        }
        Collections.sort(collection, Comparator.comparing(DigitalVideoDisc::getTitle));
        iterator = collection.iterator();
        System.out.println("The DVDs currently in the order are:");
        while (iterator.hasNext()) {
            System.out.println(iterator.next().getTitle() + " ");
        }
        sc.close();
    }
}
