public class DigitalVideoDisc extends Disc implements Playable {

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }

    public DigitalVideoDisc(String title, String category, Float cost) {
        super(title, category, cost);
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category, cost, length, director);
    }

    public DigitalVideoDisc() {
    }

    public boolean search(String title) {
        if (super.getTitle().equals(title))
            return true;
        return false;
    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + super.getTitle());
        System.out.println("DVD length: " + super.getLength());
    }

    public int compareTo(DigitalVideoDisc a) {
        String s = a.getTitle();
        String t = super.getTitle();
        if (t.compareTo(s) < 0)
            return -1;
        if (t.compareTo(s) > 0)
            return 1;
        return 0;
    }
}
