import java.util.Scanner;
import  java.util.Arrays;
public class N56{
    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number elements of arrays:");
        int n = sc.nextInt();
        int[] a= new int[n];
        int sum=0;
        float average=0;
        for(int i=0;i<n;i++)
        {
            System.out.print("Enter a["+i+"]=");
            a[i]=sc.nextInt();
            sum=sum+a[i];
            average=average+a[i];
        }
        average=average/n;
        Arrays.sort(a);
        System.out.println("Arrays after sort:");
        for(int i=0;i<n;i++)System.out.print(a[i]+" ");

        System.out.println("Sum of array:"+sum);
        System.out.println("Average of array:"+average);
        sc.close();
    }
}