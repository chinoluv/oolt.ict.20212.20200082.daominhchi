import java.util.*;

public class CompactDisc extends Disc implements Playable {
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<Track>();

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    CompactDisc(String title) {
        super(title);
    }

    CompactDisc(String title, String category) {
        super(title, category);
    }

    CompactDisc(String title, String category, float cost) {
        super(title, category, cost);
    }

    CompactDisc(String title, String category, float cost, String artist) {
        super(title, category, cost);
        this.artist = artist;
    }

    public CompactDisc() {
    }

    public void addTrack(Track a) {
        tracks.add(a);
    }

    public void removeTrack(Track a) {
        tracks.remove(a);
    }

    public int getLength() {
        return tracks.size();
    }

    @Override
    public void play() {
        for (Track item : tracks) {
            item.play();
        }
    }
}
